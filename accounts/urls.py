from django.urls import path
from accounts.views import user_login, user_logout, signup

# same info as what is on receipts.urls.py. Nothing new here.


urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup, name="signup"),
]

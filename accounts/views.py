from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from accounts.forms import LoginForm, SignUpForm
from django.contrib.auth.models import User


# Using form.cleaned_data so you arnt calling a model and always
# adding the login/signup data to the DB, instead you are just holding
# onto the pulled info to compare to djangos built in authenticator
# and using its user creator
#  Helpful Links
#  https://docs.djangoproject.com/en/4.0/topics/auth/default/#creating-users
#  https://docs.djangoproject.com/en/4.0/topics/auth/default/#how-to-log-a-user-in-1
#  https://docs.djangoproject.com/en/4.0/ref/forms/validation/


def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect("home")

    else:
        form = LoginForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,
                )
                login(request, user)

                return redirect("home")
            else:
                form.add_error("password", "Passwords do not match")
    else:
        form = SignUpForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)

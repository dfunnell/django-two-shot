from django import forms

# these forms dont require a model class because
# we are using djangos built in login and
# create user features. Only import the attributes
# required for creating the user. Can add more.
# widget allows you to use more characters for your
# password.
# https://docs.djangoproject.com/en/3.0/ref/forms/widgets/


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )


class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )

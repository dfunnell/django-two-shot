from django.apps import AppConfig

# Same thing as receipts, add this to settings.py


class AccountsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "accounts"

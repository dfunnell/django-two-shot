from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# info on adding @login_requred
# https://docs.djangoproject.com/en/4.1/topics/auth/default/#the-login-required-decorator
# filter(owner=request.user)is filtering by whoever created the category or acc
# owner is defined in models, where as request.user is the logged in user.
# redirect to "home" is defined in my urls.py
# account = form.save(False) you set it equal to a variable and add false so
# it doesnt save and you can add the user as the purchaser.
# context and return render need to be OUTSIDE OF THE ELSE STATEMENT!!!
# if it doesnt pass the if statement above, it needs to return the form again
# FYI,"receipts/list.html" is looking at the folder within templates called
# receipts, because you are currently also in receipts.views, I think it also
# just looks for folders named templates and then any subfolders in there.
# a link for date formats:
# https://docs.djangoproject.com/en/4.1/ref/templates/builtins/#date
# see CSS and HTML from scrumptious for reference


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipts,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.purchaser = request.user
            recipe.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": categories,
    }
    return render(request, "receipts/categories.html", context)


@login_required
def account(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "account_list": accounts,
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)

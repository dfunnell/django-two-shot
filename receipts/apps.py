from django.apps import AppConfig

# need to add this to installed on settings.py


class ReceiptsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "receipts"

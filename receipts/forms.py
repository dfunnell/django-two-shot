from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account

# need to import ModelForm from django.forms and import your
# model classes. Only list the fields you want on your form
# some data will be input automatically like maybe user data
# or dates for created etc...


class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]


class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]


class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]

from django.urls import path
from receipts.views import receipt_list, create_receipt, category_list
from receipts.views import account, create_category, create_account

# had to have all the imports on multiple lines because of a line too
# long from flake8
# make sure your names match what you use in html and redirects
# make sure your view functions match. Use copy paste.


urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]

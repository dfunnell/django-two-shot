from django.db import models
from django.contrib.auth.models import User

# could also use settings.AUTH_USER_MODEL instead of User
# you would need to add from django.conf import settings
# .CASCADE deletes many in one to many relationship
# models.foreignkey is relating everything to the User
# under the variable owner.
# had an issue with models.DateTimeField(), originally had
# it filled in with (auto_now_add=True) to get create now date
# but users can input old receipts so need the option to select
# def __str__(self) return self.name adds the name to the table
# examples of many to one relations below
# https://docs.djangoproject.com/en/4.1/topics/db/examples/many_to_one/


class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        User,
        related_name="categories",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        User,
        related_name="accounts",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


class Receipt(models.Model):
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(max_digits=10, decimal_places=3)
    tax = models.DecimalField(max_digits=10, decimal_places=3)
    date = models.DateTimeField()
    purchaser = models.ForeignKey(
        User,
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        "ExpenseCategory",
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    account = models.ForeignKey(
        "Account",
        related_name="receipts",
        on_delete=models.CASCADE,
        null=True
    )
